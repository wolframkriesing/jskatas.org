#!/bin/bash

ORIGIN_ROOT="."
OUTPUT_ROOT="$ORIGIN_ROOT/_output"

echo "Building for offline mode..."
echo "  - Replacing all ace online refs with offline refs."

cp -r $ORIGIN_ROOT/vendor $OUTPUT_ROOT;

# replace online refs using offline-refs (so I can work offline too)
if [[ $OSTYPE == darwin* ]]; then
  # replace the ace files with the local one in all *.html files inside the dir "katas:
  find $OUTPUT_ROOT/katas -type f -name "*.html" -exec sed -i '' "s/\/\/cdnjs.cloudflare.com\/ajax\/libs\/ace\/1.24.1/\/vendor\/ace/g" {} \;
else
  find $OUTPUT_ROOT/katas -type f -name "*.html" -exec sed -i "s/\/\/cdnjs.cloudflare.com\/ajax\/libs\/ace\/1.24.1/\/vendor\/ace/g" {} \;
fi;
