dateCreated: 2021-01-15 00:56 CET
tags: jskatas, business

# Sustainability

### Privacy

You own your data. Always.

### Open Source

The site and the code is open source. 

### How Do You Get Butter on Your Bread (or Make Money)?
