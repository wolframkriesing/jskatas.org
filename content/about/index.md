# About jskatas.org

The vision of jskatas.org in short is:  
**Continuously Learn JavaScript. Your Way.**

A bit more explicit and longer it is:   
**Learn and re-learn (all of) JavaScript, at your level and at your speed.**  
The above is shorter, more explicit and is more visionary.

No matter if you are beginner or an expert, a slow or fast learner.
The site jskatas aims to allow anyone to discover and learn unknown or new parts of JavaScript.
You can also consolidate your JavaScript knowledge or extend
your skills and sharpen them.

Learning with jskatas requires you to **actively** work with and on the language. There
is no lean-back content to "just consume" and hope to have acquired knowledge,
this is NOT what jskatas offers, and also does not want to offer.

[Read more about it in the blog post **"JSKatas.org explained"**](/blog/2021/01/09-jskatas-explained/).



## Impressum (Germany)

Angaben gemäß § 5 TMG.

### Kontakt  
E-Mail: w (has an email at) kriesing . de

Verantwortlich für den Inhalt  
Nach § 55 Abs. 2 RStV:
Wolfram Kriesing, Richard-Strauss-Str. 21, 81677 München

### Haftung für Inhalte  
Die Inhalte unserer Seiten wurden mit größter Sorgfalt erstellt. Für die Richtigkeit, Vollständigkeit und 
Aktualität der Inhalte können wir jedoch keine Gewähr übernehmen. Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG 
für eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. 
Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet, übermittelte oder gespeicherte 
fremde Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen. 
Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben 
hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten 
Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte 
umgehend entfernen.

### Haftung für Links  
Unser Angebot enthält Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss haben. 
Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten 
Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten 
wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum 
Zeitpunkt der Verlinkung nicht erkennbar. Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist 
jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen 
werden wir derartige Links umgehend entfernen.

### Datenschutz  
Die Nutzung unserer Webseite ist in der Regel ohne Angabe personenbezogener Daten möglich. Soweit auf unseren 
Seiten personenbezogene Daten (beispielsweise Name, Anschrift oder eMail-Adressen) erhoben werden, erfolgt dies, 
soweit möglich, stets auf freiwilliger Basis. Diese Daten werden ohne Ihre ausdrückliche Zustimmung nicht an 
Dritte weitergegeben.

Wir weisen darauf hin, dass die Datenübertragung im Internet (z.B. bei der Kommunikation per E-Mail) 
Sicherheitslücken aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht möglich.

Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten durch Dritte zur Übersendung von 
nicht ausdrücklich angeforderter Werbung und Informationsmaterialien wird hiermit ausdrücklich widersprochen. 
Die Betreiber der Seiten behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten Zusendung von 
Werbeinformationen, etwa durch Spam-Mails, vor.
