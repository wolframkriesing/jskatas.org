# Ideas
- Can my browser run this kata? - run the kata and show the result to see if the kata can be completed in the current browser
- Table overview of all katas, showing:
  - if they work in this browser
  - what have I finished already
  - what katas are missing
  - "How much of JS do I 'know'?"
- Group by kata-group (like "Array API", etc.)

- [ ] simplify the markdown parsing of katas, e.g. just parse H1+first-paragraph and the kata body (from H2 onwards) as markdown, not the complicated way its done now
- [ ] move the functions, which are spread across so many files into one file "load-kata", "load-kata-markdown" ... and their names mean almost nothing :(

# v2

- [x] Interactive Coding Environment
  - [x] make all katas work in the kata-editor
  - [x] track how many katas have been opened, made pass
  - [x] open the first kata on page load

# v3

- [ ] build one article about learning arrays
  - [ ] maybe just show the next part, once the last test has turned green?
  - [ ] list the TOC on the right of the article
  - [ ] fold in "less important/relevant" sections by default (e.g for creating arrays, there are many sub sections but not all are relevant for learning arrays)
  - [ ] build one popup which shows first, asks a couple questions and while answering it checks/unchecks the sections in the TOC
    - [ ] once the popup hides fold in the sections that are not checked

# v4

- [ ] improve the instructions in the test descriptions for all katas, make them all GIVEN-WHEN-THEN style
- [ ] remove the old markdown generation code, this is unused, see `loadKataSite()`
- [ ] Curated Curriculum
- [ ] Progress Tracking
- [ ] Feedback and Support
- [ ] High-Quality Content: Ensure that the content of your curriculum is of high quality and is accurate. You may also want to provide additional resources, such as links to documentation, tutorials, or articles, for users who want to learn more about a particular topic.
- [ ] Marketing: Promote your website on social media, coding forums, and other online communities where potential users might hang out. You may also want to consider running online advertisements or collaborating with other websites or influencers in the coding community.
- [ ] Feedback Loop: Regularly collect feedback from your users and use this feedback to improve your website. You may also want to conduct user testing to identify any usability issues or areas for improvement.

# JSChronicle 
- [ ] show progress bar
- [ ] add sharing buttons
- [x] update URL hash, so one can also link a side directly
- [x] add tracking, so I see to which depth people used it
- [x] fix mobile!
- [x] remove "hidden" content, show it right away
- [x] dont override the back button behavior

- [ ] add removed properties
- [ ] type all the es versions JSON

# More (ideas)

- [ ] handle terms
  - [ ] list all "terms" on one site and explain them
  - [ ] link all terms (in test descriptions), maybe look at <dfn> and <a>+aria-describedby 
- [x] make the home page more the main page not the same as the group page, for the search engines!
- [x] ~~make the blog post preview work (see mastodon, it currently doesnt work)~~ it just used a different link than I thought :)
- [ ] make "learn array sort" and alike show up jskatas as a search result, currently for "jskatas array sort" is at the top, but thats not generic enough and never searched
- [x] make the sub navigation more prominent, in the end this is more interesting than "blog" and "about" page
- [ ] add a /explained URL which points to the blog post????
- [ ] make the home page be a schema-type "Course"
- [ ] schema.org stuff
  - [ ] add basic <meta> data for the site (see picostitch)
  - [ ] make "JavaScript - Learn with Katas" and "Learn and re-learn (all of) JavaScript, at your level and at your speed" be what is shown on google as preview snippet for the site
  - [ ] add rich snippet for the home page
  - [ ] add rich snippet for each kata pages
  - [ ] add rich snippet for each kata group pages
- [ ] test explainers "what is a kata", "how can i learn here", ... "what is a kata group", "what is a kata bundle"


# One page per kata (for explicit pages, SEO, insights on one page and link targets)

- [x] open kata's tddbin page
- [x] fix the H2 anchors on the overview pages
- [x] improve the katas content (the describe+it in the katas), which land on each kata's site
- [x] show the right metadata on the kata sites (links, related katas, level, ...)
- [x] fix the `twitter:title`, its currently `undefined`
- [x] move the parseint link into the metadata, not hardcoded in the template
- [x] take it live (using codeberg pages?)
- [x] generate but dont link the es6/es2020 pages twice

- [x] make `/katas` the homepage, use the same canonical (for now)
- [x] generate all kata pages
  - [x] provide the md content via the KataMarkdownFile object
  - [x] a KataSite should require the `Kata`, `KataMarkdownFile` and maybe the `KataBundle` object
  - [x] ~~generate~~ provide all data for the md (files/string) for the kata pages
    - [x] provide a local script to convert test into md file
    - [x] generate the headers for the MD file, the H1 and the tags and attributes above it
    - [x] remove requirement for strange md-file prefix (which used to be useful for the blog, iirc)
    - [x] generate the md files for any number of tests (so we can later just loop over it when processing the meta files)
  - [x] provide all the tests+meta data to render the kata page
    - [x] get all meta files
      - [x] provide the locations of all meta files to be found under a certain root-URL
      - [x] download the all into a local cache (so we dont have to redo it every time), invalidate cache just by removing it
    - [x] provide a JS interface to the metadata
    - [x] loop over all metadata objects
    - [x] provide the md (string) from the tests+metadata
    - [x] generate the site (HTML) for per kata
    - [x] write it into the _output directory
    - [x] make this process run in the index.js when the server runs and update when data changes
  - [x] make backticks in kata titles render into the md file (some bash magic prevents this currently :( )
  - [x] provide the H1 for a md file, use the `groupName`+`name` of the kata
- [x] add types, strict and relentless (I know that would mean use rescript ... <excuse1>, <excuse2>, ...)
