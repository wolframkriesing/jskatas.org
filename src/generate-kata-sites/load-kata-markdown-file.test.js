// @ts-nocheck TODO make this type checked, but for getting 0 errors for now, some files dont get checked yet. Improve it!
import {strict as assert} from 'assert';
import {assertThat, hasProperties, instanceOf} from 'hamjest';

import {describe, it} from '../test.js';

import {loadManyKataMarkdownFiles} from './load-kata-markdown-file.js';
import {KataMarkdownFile} from "./KataMarkdownFile.js";

describe('Load kata-markdown files from a directory', () => {
  describe('GIVEN a list of files', () => {
    const loadManyKataMdFiles = async (files) => {
      const findKataSourceFilenames = async () => files;
      const readFile = async () => {};
      const load = loadManyKataMarkdownFiles({findKataSourceFilenames, readFile});
      return await load();
    };
    it('WHEN the list is empty THEN no KataMarkdownFile is returned', async () => {
      const noFiles = [];
      const manySourceFiles = await loadManyKataMdFiles(noFiles);
      assert.deepEqual(manySourceFiles, []);
    });
    it('WHEN one file is given THEN return one KataMarkdownFile', async () => {
      const mdFiles = await loadManyKataMdFiles(['es1/global-object/parseint.md']);
      assert.equal(mdFiles.length, 1);
      assertThat(mdFiles[0], instanceOf(KataMarkdownFile));
      assertThat(mdFiles[0], hasProperties({
        filename: 'es1/global-object/parseint.md',
      }));
    });
    it('WHEN multiple files are given THEN return all KataMarkdownFiles', async () => {
      const files = [
        '/es1/global-object/unary-operators.md',
        '/es6/number-api/isinteger.md',
      ];
      const manySourceFiles = await loadManyKataMdFiles(files);
      assert.equal(manySourceFiles.length, 2);
      assertThat(manySourceFiles[0], hasProperties({filename: files[0]}));
      assertThat(manySourceFiles[1], hasProperties({filename: files[1]}));
    });
  });
});
