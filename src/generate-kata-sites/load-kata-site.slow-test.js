// @ts-nocheck TODO make this type checked, but for getting 0 errors for now, some files dont get checked yet. Improve it!
import * as path from 'path';
import {assertThat, hasProperties} from 'hamjest';

import {describe, it} from '../test.js';
import {TEST_CONTENT_DIRECTORY} from '../config.js';
import {loadManyKataMarkdownFiles} from './load-kata-markdown-file.js';
import {loadManyKataSites} from './load-kata-site.js';

const katasDirectory = path.join(TEST_CONTENT_DIRECTORY, 'katas');

describe('Build posts from real files (tests are slow therefore)', () => {
  it('GIVEN one file WHEN loading works THEN return a complete KataSite object', async () => {
    const kataMarkdownFiles = await loadManyKataMarkdownFiles()(katasDirectory);
    const posts = await loadManyKataSites()(kataMarkdownFiles);

    const expectedAbstract = `<code>parseInt()</code> parses a string and returns an integer.`;
    assertThat(posts[0], hasProperties({
      dateCreated: '2021-12-26T13:56:42.025Z',
      headline: 'Global Object API: `parseInt()`',
      url: '/katas/es1/global-object/parseint/',
      abstractAsHtml: expectedAbstract,
    }));
  });
});
