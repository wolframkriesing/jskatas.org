import {KataBundleId} from "./KataBundle";

type RawKataFromMetadata = {
  id: number;
  name: string;
  description: string;
  path: string;
  level: string;
  requiresKnowledgeFrom: any[],
  groupName: string;
  groupNameSlug: string;

  links?: any[],
  publishDateRfc822?: string,
};
type RawKataLink = {
  url: string;
  comment: string;
  tags?: string[];
};
type RawKataId = {
  id: number;
  bundle: string;
};

type KataId = {
  bundle: string;
  id: number;
};
type KataLink = {
  url: string;
  comment: string;
  tags: string[];
};
type KataConfig = {
  bundleId: KataBundleId;
  rootUrl?: string;
};

type KataStats = {
  numTests: number;
}

export class Kata {
  id: KataId;
  name: string;
  description: string;
  isPublished: boolean;
  links: KataLink[];
  url: RelativeUrl;
  groupName: string;
  level: string;
  readableCreatedAt: string;
  requiresKnowledgeFrom: KataId[];
  absolutePath: string;
  path: Path;
  stats?: KataStats;
  static fromRawMetadataObject: (obj: RawKataFromMetadata, config: KataConfig) => Kata;
  isEqual: (kata: Kata) => boolean;
}

type ForTestingOnly = {
  fixtures: {
    oneRawKataObject: (partial?: Partial<RawKataFromMetadata>) => RawKataFromMetadata;
    oneRawKataLink: (partial?: Partial<RawKataLink>) => RawKataLink;
  }
};
export const forTestingOnly: ForTestingOnly;
