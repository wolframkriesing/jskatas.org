// @ts-nocheck TODO make this type checked, but for getting 0 errors for now, some files dont get checked yet. Improve it!
import {KataSite} from "./KataSite";

type Deps1 = {
  readFile,
}

// export function loadManyKataSites(deps: Deps1):
//   (manyKataMarkdownFiles: ???[]) => Promise<???>;

export function loadKataSite(deps: Deps1):
  (sourceFile) => Promise<KataSite>
