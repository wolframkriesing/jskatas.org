import {KataMarkdownFile} from "./KataMarkdownFile";

export type RawKataSite = {
  abstract: string;
  abstractAsHtml: string;
  bodyAsHtml: string;
  hasAbstractOnly: boolean;
  headline: string;
  headlineAsHtml: string;
};

export type KataSiteMetadata = {
  canonicalUrl: string;
  canonicalHint: string;
  dateCreated: DateString | DateTimeString;
  isDraft: boolean;
  oldUrls: string[];
  previewImage: string;
  tags: string[];
  vimeoId?: string;
  videoStartTime?: string;
  youtubeId?: string;
};

export class KataSite implements Article {
  private _dateCreated: KataSiteMetadata['dateCreated'];
  _rawTags: string[];
  abstract: RawKataSite['abstract'];
  abstractAsHtml: RawKataSite['abstractAsHtml'];
  bodyAsHtml: RawKataSite['bodyAsHtml'];
  canonicalUrl: KataSiteMetadata['canonicalUrl'];
  canonicalHint: KataSiteMetadata['canonicalHint'];
  dateCreated: KataSiteMetadata['dateCreated'];
  hasVideo: boolean;
  headline: string;
  headlineAsHtml: string;
  isDraft: KataSiteMetadata['isDraft'];
  markdownFile: KataMarkdownFile;
  oldUrls:  KataSiteMetadata['oldUrls'];
  tags:  Tag[];
  previewImageUrl: KataSiteMetadata['previewImage'];
  url: string;
  vimeoId?: KataSiteMetadata['vimeoId'];
  videoStartTime?: KataSiteMetadata['videoStartTime'];
  youtubeId?: KataSiteMetadata['youtubeId'];

  static fromKataMarkdownFile(kataMarkdownFile: KataMarkdownFile, rawKataSite: RawKataSite): KataSite;
  static withRawData(rawKataSite: RawKataSite): KataSite;
}
