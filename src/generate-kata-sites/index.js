// @ts-nocheck this is still an experiment
import * as path from 'path';

import {testFileToMarkdown} from "./load-kata.js";
import {loadKataSite} from "./load-kata-site.js";
import {KataMarkdownFile} from "./KataMarkdownFile.js";
import {renderTemplate} from "../_deps/render-template.js";
import {writeOutputFile} from "../_deps/fs.js";
import {findAlikeKataGroups, findRequiredKnowledgeKatas} from "./queries.js";

const generateKataPage = async (site, kata, alikeKataGroups, requiredKnowledgeKatas, breadCrumbs, pageParams) => {
  const data = {...pageParams, kataData: kata, breadCrumbs, page: site, pageTitlePrefix: `${kata.groupName} – ${kata.name}`, alikeKataGroups, requiredKnowledgeKatas};
  const fileContent = await renderTemplate('katas/kata.html', data);

  await writeOutputFile(`${site.url}/index.html`, fileContent);
}

const sortByItemsCount = (a, b) => a.length < b.length ? 1 : -1;
const sortByKatasCount = (a, b) => a.katas.length < b.katas.length ? 1 : -1;

const generateKatasOverviewPage = async (kataBundles, pageParams) => {
  const data = {...pageParams, kataBundles, subfilter: 'chronological', pageTitlePrefix: 'Katas Chronological'};
  const fileContent = await renderTemplate('katas/versions.html', data);
  await writeOutputFile('katas/versions/chronological/index.html', fileContent);

  {
    const kataBundlesView = kataBundles.sort(sortByKatasCount);
    const data = {...pageParams, kataBundles: kataBundlesView, subfilter: 'popular', pageTitlePrefix: 'Popular Katas'};
    const fileContent = await renderTemplate('katas/versions.html', data);
    await writeOutputFile('katas/versions/popular/index.html', fileContent);
    await writeOutputFile('katas/versions/index.html', fileContent);
  }
}

const countKatas = (kataGroups) => {
  return kataGroups.map(group => group.length).reduce((prev, cur) => prev+cur, 0)
};

export const generateKatasByGroupOverviewPage = async ({kataGroups, pageParams}) => {
  const data = {...pageParams, kataGroups, subfilter: 'all', katasCount: countKatas(kataGroups), pageTitlePrefix: 'Kata Categories'};
  const fileContent = await renderTemplate('katas/groups.html', data);
  await writeOutputFile('katas/groups/index.html', fileContent);
  await writeOutputFile('katas/groups/all/index.html', fileContent);
  await writeOutputFile('katas/index.html', fileContent);

  const isLevel = {
    easy: kata => kata.level === 'BEGINNER',
    hard: kata => kata.level === 'EXPERT' || kata.level === 'ADVANCED',
    medium: kata => isLevel.easy(kata) === false && isLevel.hard(kata) === false,
  };
  {
    const filteredKataGroups = kataGroups.map(group => {
      return group.filter(isLevel.easy)
    })
      .filter(group => group.length > 0)
      .sort(sortByItemsCount);
    const data = {...pageParams, kataGroups: filteredKataGroups, subfilter: 'easy', katasCount: countKatas(filteredKataGroups), pageTitlePrefix: 'Easy Katas'};
    const fileContent = await renderTemplate('katas/groups.html', data);
    await writeOutputFile('katas/groups/level-easy/index.html', fileContent);
  }

  {
    const filteredKataGroups = kataGroups.map(group => {
      return group.filter(isLevel.hard)
    })
      .filter(group => group.length > 0)
      .sort(sortByItemsCount);
    const data = {...pageParams, kataGroups: filteredKataGroups, subfilter: 'hard', katasCount: countKatas(filteredKataGroups), pageTitlePrefix: 'Hard Katas'};
    const fileContent = await renderTemplate('katas/groups.html', data);
    await writeOutputFile('katas/groups/level-hard/index.html', fileContent);
  }

  {
    // all the rest
    const filteredKataGroups = kataGroups.map(group => {
      return group.filter(isLevel.medium)
    })
      .filter(group => group.length > 0)
      .sort(sortByItemsCount);
    const data = {...pageParams, kataGroups: filteredKataGroups, subfilter: 'medium', katasCount: countKatas(filteredKataGroups), pageTitlePrefix: 'Medium Katas'};
    const fileContent = await renderTemplate('katas/groups.html', data);
    await writeOutputFile('katas/groups/level-medium/index.html', fileContent);
  }
}

export const generateHomePage = async ({kataGroups, pageParams}) => {
  const data = {
    ...pageParams,
    kataGroups,
    katasCount: countKatas(kataGroups),
    pageTitlePrefix: 'Home'
  };
  const fileContent = await renderTemplate('index.html', data);
  await writeOutputFile('index.html', fileContent);
}

export const generateTryPage = async ({kataGroups, pageParams}) => {
  const data = {
    ...pageParams,
    kataGroups,
    katasCount: countKatas(kataGroups),
    pageTitlePrefix: 'Try'
  };
  const fileContent = await renderTemplate('try.html', data);
  await writeOutputFile('try/index.html', fileContent);
}

export const generateKataSites = async ({ pageParams, kataRoot, bundles }) => {
  return Promise.all([
    generateKatasOverviewPage(bundles, pageParams),
    ...bundles.map(bundle =>
      bundle.katas.map(async kata => {
        const title = `${kata.groupName}: ${kata.name}`;
        const subtitle = kata.description;

        const kataFilename = path.join(kataRoot, bundle.rootUrl, `${kata.path}.js`);
        const markdownFilename = path.join(kataRoot, bundle.rootUrl, `${kata.path}.md`);
        const {markdown, stats, tests} = await testFileToMarkdown()({fileName: kataFilename, title, subtitle})
        kata.stats = stats; // This might not be the ideal place for it, better would be when loading the kata to also load the file and therefore the stats.

        const kataMarkdownFile = KataMarkdownFile.fromFile(kataRoot, markdownFilename, markdown);
        const site = await loadKataSite()(kataMarkdownFile, tests);

        const breadCrumbs = [
          {title: 'All katas', url: '/katas/'},
          {title: bundle.name/*, url: '/katas/es1/'*/},
          {title: kata.groupName/*, url: '/katas/es1/global-api/'*/},
          {title: kata.name},
        ];

        const alikeKataGroups = findAlikeKataGroups(bundles, kata);
        const requiredKnowledgeKatas = findRequiredKnowledgeKatas(bundles, kata);
        await generateKataPage(site, kata, alikeKataGroups, requiredKnowledgeKatas, breadCrumbs, pageParams);
      })
    )]
  );
};
