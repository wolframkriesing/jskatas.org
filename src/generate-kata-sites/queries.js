/**
 * @typedef {import('./KataBundle').KataBundle} KataBundle
 * @typedef {import('./KataBundle').KataGroup} KataGroup
 * @typedef {import('./Kata').Kata} Kata
 */

/**
 * @param bundles {KataBundle[]}
 * @param kata {Kata}
 * @return kata {Kata[]}
 */
export const findRequiredKnowledgeKatas = (bundles, kata) => {
  const requiredIds = kata.requiresKnowledgeFrom;
  /** @type {function(Kata): boolean} */
  const isInRequiredKnowledge = (k) => {
// TODO this MUST go into the Kata, the comparison if a kata is a requiredKnowledge
    const kataIds = requiredIds.filter(id => id.id===k.id.id && id.bundle===k.id.bundle);
    return kataIds.length > 0;
  };
  if (bundles.length) {
    const katas = bundles.map(b => b.katas).flat();
    return katas.filter(isInRequiredKnowledge);
  }
  return [];
};

/**
 * @param bundles {KataBundle[]}
 * @param kata {Kata}
 * @return {KataGroup[]}
 */
export const findAlikeKataGroups = (bundles, kata) => {
  const stopWords = ['api'];
  const ignoreStopWords = (s) => false === stopWords.includes(s);
  const removeTrailingS = (s) => s.endsWith('s') ? s.slice(0, -1) : s;

    const findStringsInGroupName = kata.groupName.toLowerCase()
    .split(' ')
    .map(removeTrailingS)
    .filter(ignoreStopWords)
  ;
  
  const allAlikeKatas = bundles.map(b => b.katas).flat()
    .filter(kata => kata.isPublished)
    .filter(kata => findStringsInGroupName.some(s => kata.groupName.toLowerCase().includes(s)));
  return groupKatasByGroupName(allAlikeKatas);
}

/**
 * @param katas {Kata[]}
 * @return {KataGroup[]}
 */
const groupKatasByGroupName = (katas) => {
  const groups = new Map();
  katas.forEach((kata) => {
    if (groups.has(kata.groupName) === false) {
      groups.set(kata.groupName, []);
    }
    groups.get(kata.groupName).push(kata);
  });
  return [...groups.values()];
}
