// @ts-nocheck TODO make this type checked, but for getting 0 errors for now, some files dont get checked yet. Improve it!
import * as marked from 'marked';
import {KataSite} from './KataSite.js';
import {readFile} from '../_deps/fs.js';
import {parseMetadata} from '../_shared/parse-metadata.js';
import {findNextParagraphTokens, renderAbstractAsHtml, renderHeadlineAsHtml} from '../_shared/markdown.js';
import {tokensToHtml} from '../_shared/more-html-markdown.js';

const prodDeps = () => {
  return {readFile};
};

const findHeadlineAndAbstract = (tokens) => {
  let tokenIndex = 0;
  let headlineTokens = [];
  while (tokenIndex < tokens.length && headlineTokens.length === 0) {
    const t = tokens[tokenIndex];
    if (t.type === 'heading' && t.depth === 1) headlineTokens = [t];
    tokenIndex++;
  }
  const abstractTokens = findNextParagraphTokens(tokens.slice(tokenIndex));
  return {headlineTokens, abstractTokens};
}

const metadataParseConfigs = [
  {key: 'canonicalUrl', type: 'string'},
  {key: 'canonicalHint', type: 'string'},
  {key: 'dateCreated', type: 'string'},
  {key: 'isDraft', type: 'boolean'},
  {key: 'oldUrls', type: 'array', separator: ' '},
  {key: 'tags', type: 'array', separator: ','},
  {key: 'previewImage', type: 'string'},
  {key: 'videoStartTime', type: 'string'},
  {key: 'vimeoId', type: 'string'},
  {key: 'youtubeId', type: 'string'},
];
const parseRawPost = tokens => {
  const {headlineTokens, abstractTokens} = findHeadlineAndAbstract(tokens);
  const metadata = parseMetadata(tokens[0], metadataParseConfigs);
  return {
    headline: headlineTokens[0].text,
    headlineAsHtml: renderHeadlineAsHtml(headlineTokens[0]),
    abstract: abstractTokens.length > 0 ? abstractTokens[0].text : '',
    abstractAsHtml: renderAbstractAsHtml(abstractTokens),
    ...metadata
  };
};
const findBodyToRender = tokens => {
  // DANGER we are modifying `tokens` here, since it has some properties, like `links`
  // set on the object, so it's not a pure array ... therefore we rather just shift() out
  // elements, instead of cloning it and may fuck up something else of marked's tokens object.
  while (tokens.length > 0) {
    if (tokens[0].type === 'heading' && tokens[0].depth === 1) {
      tokens.shift();
      tokens.shift();
      return;
    }
    tokens.shift();
  }
}
const renderBodyAsHtml = tokens => {
  findBodyToRender(tokens);
  return tokensToHtml(tokens);
}

export const loadManyKataSites = () => async manyKataMarkdownFiles => {
  return Promise.all(manyKataMarkdownFiles.map(loadKataSite()));
};

export const loadKataSite = () => async (kataMarkdownFile, tests) => {
  const tokens = marked.lexer(kataMarkdownFile.markdownText);
  const parsedPostData = parseRawPost(tokens);
  const bodyAsHtml = renderBodyAsHtml(tokens);
  return KataSite.fromKataMarkdownFile(kataMarkdownFile, {...parsedPostData, bodyAsHtml, tests})
}
