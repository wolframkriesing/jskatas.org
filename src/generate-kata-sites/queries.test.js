import {describe, it} from '../test.js';
import {assertThat, contains, hasProperties} from 'hamjest';
import {forTestingOnly, KataBundle} from './KataBundle.js';
import {forTestingOnly as forTestingKataOnly, Kata} from './Kata.js';
import {findAlikeKataGroups, findRequiredKnowledgeKatas} from "./queries.js";

/**
 * @typedef {import('./KataBundle').RawKataBundleFromMetadata} RawKataBundleFromMetadata
 * @typedef {import('./Kata').RawKataFromMetadata} RawKataFromMetadata
 */

const { oneRawKataBundleObject } = forTestingOnly.fixtures;
const { oneRawKataObject } = forTestingKataOnly.fixtures;

/** @type {function(RawKataFromMetadata): Kata} */
const oneKata = (raw) => Kata.fromRawMetadataObject(raw, {bundleId: ''});
/** @type {function(RawKataBundleFromMetadata): KataBundle} */
const oneBundle = (raw) => KataBundle.fromRawMetadataObject(raw, {bundleId: ''});

describe('Find required-knowledge katas, for a given kata', () => {
  it('WHEN a kata has no required-knowledge katas THEN the list is empty', () => {
    const kata = Kata.fromRawMetadataObject(oneRawKataObject({requiresKnowledgeFrom: []}), {bundleId: ''});
    assertThat(findRequiredKnowledgeKatas([], kata), []);
  });
  it('WHEN kataA is required by kataB THEN return an array with kataA', () => {
    const bundleId = 'the bundle ID';
    const rawKataA = oneRawKataObject({name: 'kataA', id: 1});
    const rawKataB = oneRawKataObject({name: 'kataB', id: 2, requiresKnowledgeFrom: [{bundle: bundleId, id: 1}]});
    const bundle = KataBundle.fromRawMetadataObject(oneRawKataBundleObject({items: [rawKataA, rawKataB]}), {bundleId});
    const kataB = Kata.fromRawMetadataObject(rawKataB, {bundleId});
    assertThat(
      findRequiredKnowledgeKatas([bundle], kataB),
      contains(hasProperties({name: 'kataA'}))
    );
  });
  it('WHEN multiple katas are required-knowledge THEN provide them', () => {
    const bundle1Id = 'the bundle ONE ID';
    const bundle2Id = 'the bundle TWO ID';
    const rawKataA = oneRawKataObject({name: 'kata1.A', id: 1});
    const rawKataB = oneRawKataObject({name: 'kata1.B', id: 2});
    const rawKataC = oneRawKataObject({name: 'kata2.C', id: 1});
    const rawKataD = oneRawKataObject({name: 'kata2.B', id: 2, requiresKnowledgeFrom: [
      {bundle: bundle1Id, id: 1}, {bundle: bundle1Id, id: 2}, {bundle: bundle2Id, id: 1}
    ]});
    const bundle1 = KataBundle.fromRawMetadataObject(oneRawKataBundleObject({items: [rawKataA, rawKataB]}), {bundleId: bundle1Id});
    const bundle2 = KataBundle.fromRawMetadataObject(oneRawKataBundleObject({items: [rawKataC, rawKataD]}), {bundleId: bundle2Id});
    const kataD = Kata.fromRawMetadataObject(rawKataD, {bundleId: bundle2Id});
    assertThat(
      findRequiredKnowledgeKatas([bundle1, bundle2], kataD),
      contains(hasProperties({name: 'kata1.A'}), hasProperties({name: 'kata1.B'}), hasProperties({name: 'kata2.C'})),
    );
  });
});

describe('Find kata groups with related katas (e.g. for a kata in "Function API" find all groups "function", "async function", "Function constructor", etc.)', () => {
  describe('GIVEN one kata in one group', () => {
    it('WHEN searching for this one THEN this one and all katas with the same group name are returned', () => {
      const kataItems1 = [
        oneRawKataObject({name: 'a', groupName: 'function API'}),
        oneRawKataObject({name: 'b', groupName: 'function API'}),
      ];
      const bundles = [
        oneBundle(oneRawKataBundleObject({ items: kataItems1 })),
      ];
      const kata = oneKata(oneRawKataObject(kataItems1[0]));
      assertThat(findAlikeKataGroups(bundles, kata)[0][0].groupName, 'function API');
    });
    it('... even when the katas are in different bundles', () => {
      const kataItems1 = [
        oneRawKataObject({name: 'a', groupName: 'function API'}),
        oneRawKataObject({name: 'b', groupName: 'function API'}),
      ];
      const kataItems2 = [
        oneRawKataObject({name: 'c', groupName: 'function API'}),
        oneRawKataObject({name: 'd', groupName: 'function API'}),
      ];
      const bundles = [
        oneBundle(oneRawKataBundleObject({ items: kataItems1 })),
        oneBundle(oneRawKataBundleObject({ items: kataItems2 })),
      ];
      const kata = oneKata(oneRawKataObject(kataItems1[0]));
      assertThat(findAlikeKataGroups(bundles, kata)[0], contains(
        hasProperties({ name: 'a', groupName: 'function API' }),
        hasProperties({ name: 'b', groupName: 'function API' }),
        hasProperties({ name: 'c', groupName: 'function API' }),
        hasProperties({ name: 'd', groupName: 'function API' }),
      ));
    });
  });

  describe('GIVEN we search for one kata and have many different kata groups', () => {
    it('WHEN the search sees them as NOT related THEN return only the kata group of the kata we search', () => {
      const kataItems1 = [
        oneRawKataObject({name: 'a', groupName: 'function API'}),
        oneRawKataObject({name: 'b', groupName: 'import API'}),
      ];
      const kataItems2 = [
        oneRawKataObject({name: 'c', groupName: 'Promise API'}),
        oneRawKataObject({name: 'd', groupName: 'function API'}),
      ];
      const bundles = [
        oneBundle(oneRawKataBundleObject({ items: kataItems1 })),
        oneBundle(oneRawKataBundleObject({ items: kataItems2 })),
      ];
      const kata = oneKata(oneRawKataObject(kataItems1[0]));
      const found = findAlikeKataGroups(bundles, kata);
      assertThat(found, hasProperties({ length: 1 }));
      assertThat(found[0], contains(
        hasProperties({ name: 'a', groupName: 'function API' }),
        hasProperties({ name: 'd', groupName: 'function API' }),
      ));
    });
  });

  describe('related search matches', () => {
    it('WHEN the given kata\'s group name is "function API" THEN ignore the "API" and find all other groups with "function" in them', () => {
      const kataItems1 = [
        oneRawKataObject({name: 'a', groupName: 'function API'}),
        oneRawKataObject({name: 'b', groupName: 'async functions'}),
      ];
      const kataItems2 = [
        oneRawKataObject({name: 'c', groupName: 'Function constructor'}),
        oneRawKataObject({name: 'd', groupName: 'Function constructor'}),
        oneRawKataObject({name: 'e', groupName: 'Promise constructor'}),
      ];
      const bundles = [
        oneBundle(oneRawKataBundleObject({ items: kataItems1 })),
        oneBundle(oneRawKataBundleObject({ items: kataItems2 })),
      ];
      const kata = oneKata(oneRawKataObject(kataItems1[0]));
      const found = findAlikeKataGroups(bundles, kata);
      assertThat(found, hasProperties({ length: 3 }));
      assertThat(found[0], contains(
        hasProperties({ name: 'a', groupName: 'function API' }),
      ));
      assertThat(found[1], contains(
        hasProperties({ name: 'b', groupName: 'async functions' }),
      ));
      assertThat(found[2], contains(
        hasProperties({ name: 'c', groupName: 'Function constructor' }),
        hasProperties({ name: 'd', groupName: 'Function constructor' }),
      ));
    });
    it('... also many bundles work', () => {
        const bundles = [
          oneBundle(oneRawKataBundleObject({ items: [oneRawKataObject({name: 'a', groupName: 'function API'})]})),
          oneBundle(oneRawKataBundleObject({ items: [oneRawKataObject({name: 'b', groupName: 'function API'})]})),
          oneBundle(oneRawKataBundleObject({ items: [oneRawKataObject({name: 'c', groupName: 'Function constructor'})]})),
          oneBundle(oneRawKataBundleObject({ items: [oneRawKataObject({name: 'd', groupName: 'async Function constructor'})]})),
          oneBundle(oneRawKataBundleObject({ items: [oneRawKataObject({name: 'e', groupName: 'Promises will be ignored'})]})),
          oneBundle(oneRawKataBundleObject({ items: [oneRawKataObject({name: 'f', groupName: 'Will also be ignored'})]})),
        ];
        const kata = oneKata(oneRawKataObject([
            oneRawKataObject({name: 'a', groupName: 'function API'}),
        ][0]));
        const found = findAlikeKataGroups(bundles, kata);
        assertThat(found, hasProperties({ length: 3 }));
        assertThat(found[0], contains(
            hasProperties({ name: 'a', groupName: 'function API' }),
            hasProperties({ name: 'b', groupName: 'function API' }),
        ));
        assertThat(found[1], contains(hasProperties({ name: 'c', groupName: 'Function constructor' })));
        assertThat(found[2], contains(hasProperties({ name: 'd', groupName: 'async Function constructor' })));
    });
      it('AND ignore unpublished (=draft) katas', () => {
          const kata1 = oneRawKataObject({name: 'a', groupName: 'function API'});
          const kata2 = oneRawKataObject({name: 'b', groupName: 'function API'});
          const kata3 = oneRawKataObject({name: 'c', groupName: 'Function constructor'});
          delete kata1.publishDateRfc822;
          delete kata3.publishDateRfc822;
          const bundles = [
            oneBundle(oneRawKataBundleObject({ items: [kata1]})),
            oneBundle(oneRawKataBundleObject({ items: [kata2]})),
            oneBundle(oneRawKataBundleObject({ items: [kata3]})),
          ];
          const kata = oneKata(oneRawKataObject({name: 'a', groupName: 'function API'}));
          const found = findAlikeKataGroups(bundles, kata);
          assertThat(found, hasProperties({ length: 1 }));
          assertThat(found[0], contains(
              hasProperties({ name: 'b', groupName: 'function API' }),
          ));
      });
      it('WHEN searching for "arrow function" THEN find also all the groups with "function" in them', () => {
        const kataItems1 = [
          oneRawKataObject({name: 'a', groupName: 'function API'}),
          oneRawKataObject({name: 'b', groupName: 'async functions'}),
        ];
        const kataItems2 = [
          oneRawKataObject({name: 'c', groupName: 'Function constructor'}),
          oneRawKataObject({name: 'd', groupName: 'Function constructor'}),
          oneRawKataObject({name: 'e', groupName: 'Promise constructor'}),
        ];
        const bundles = [
          oneBundle(oneRawKataBundleObject({ items: kataItems1 })),
          oneBundle(oneRawKataBundleObject({ items: kataItems2 })),
        ];
        const kata = oneKata(oneRawKataObject({name: 'basics', groupName: 'arrow functions'}));
        const found = findAlikeKataGroups(bundles, kata);
        assertThat(found, hasProperties({ length: 3 }));
        assertThat(found[0], contains(
          hasProperties({ name: 'a', groupName: 'function API' }),
        ));
        assertThat(found[1], contains(
          hasProperties({ name: 'b', groupName: 'async functions' }),
        ));
        assertThat(found[2], contains(
          hasProperties({ name: 'c', groupName: 'Function constructor' }),
          hasProperties({ name: 'd', groupName: 'Function constructor' }),
        ));
      });
  });
});
