export class KataMarkdownFile {
  filename: Filename;
  kataRoot: Path;
  markdownText: string;

  static fromFile(kataRoot: Path, filename: Filename, markdownText: string): KataMarkdownFile;
}
