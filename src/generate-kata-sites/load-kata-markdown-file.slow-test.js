import * as path from 'path';
import {strict as assert} from 'assert';
import {assertThat, not, hasItem, hasProperty, endsWith} from 'hamjest';

import {describe, it} from '../test.js';
import {loadManyKataMarkdownFiles} from './load-kata-markdown-file.js';
import {TEST_CONTENT_DIRECTORY} from '../config.js';

const katasDirectory = path.join(TEST_CONTENT_DIRECTORY, 'katas');

describe('Load kata-markdown files from a directory (tests are slow, working against a real fs)', () => {
  it('GIVEN directory WHEN loading the files THEN return source file with correct `filename`', async () => {
    const katas = await loadManyKataMarkdownFiles()(katasDirectory);
    assert(katas.length > 0);
    assert.equal(katas[0].filename, `${katasDirectory}/es1/global-object/parseint.md`);
    assert.equal(katas[0].kataRoot, katasDirectory);
  });
  it('GIVEN an invalid kata-markdown file (not ending in .md) THEN don`t find it', async () => {
    const katas = await loadManyKataMarkdownFiles()(katasDirectory);
    assertThat(katas, not(hasItem(hasProperty('filename', endsWith('es1/global-object/parseint.txt')))));
    // how can we write this test better, it tests something that is not there ... kinda stupid :)
  });
});
