import * as path from 'path';
import {checkObject} from "./_checkObject.js";

/**
 * @typedef {import('./KataBundle').KataBundleId} KataBundleId
 * @typedef {import('./Kata').RawKataFromMetadata} RawKataFromMetadata
 * @typedef {import('./Kata').RawKataLink} RawKataLink
 * @typedef {import('./Kata').RawKataId} RawKataId
 * @typedef {import('./Kata').KataLink} KataLink
 * @typedef {import('./Kata').KataId} KataId
 */

/**
 * @param maybeLinks {any}
 * @return {KataLink[]}
 */
const extractLinks = (maybeLinks) => {
  if (maybeLinks === undefined) {
    return [];
  }
  if (Array.isArray(maybeLinks) === false) {
    throw new TypeError('Wrong type in KataLink object, should be like so: links (array)');
  }
  /** @type {function(RawKataLink): void} */
  const checkIncomingData = (link) => {
    const hasTags = Reflect.has(link, 'tags');
    const optionalProps = hasTags ? {tags: []} : {};
    const checkResult = checkObject(link, {...requiredProps.KataLink, ...optionalProps});
    if (checkResult.isValid === false) {
      if (checkResult.missing.length > 0) {
        throw new TypeError('Missing props in KataLink object: ' + checkResult.missing.join(', '));
      }
      if (checkResult.wrongTypes.length > 0) {
        throw new TypeError('Wrong types in KataLink object, should be like so: ' + checkResult.wrongTypes.join(', '));
      }
    }
  };
  maybeLinks.forEach(checkIncomingData);
  return maybeLinks;
};

/**
 * @param rawKataIds {RawKataId[]}
 * @return {KataId[]}
 */
const extractRequiredKnowledge = (rawKataIds) => {
  /** @type {function(RawKataId): void} */
  const checkIncoming = (rawKataId) => {
    const checkResult = checkObject(rawKataId, requiredProps.KataId);
    if (checkResult.isValid === false) {
      if (checkResult.missing.length > 0) {
        throw new TypeError('Missing props in KataId object: ' + checkResult.missing.join(', '));
      }
      if (checkResult.wrongTypes.length > 0) {
        throw new TypeError('Wrong types in KataId object, should be like so: ' + checkResult.wrongTypes.join(', '));
      }
    }
  };
  rawKataIds.forEach(checkIncoming);
  return rawKataIds;
};

export class Kata {
  /**
   * @param obj {RawKataFromMetadata}
   * @param configParam {import('./Kata').KataConfig}
   * @return {import('./Kata').Kata}
   * @constructor
   */
  static fromRawMetadataObject(obj, configParam) {
    const defaultConfig = { rootUrl: '' };
    const config = {...defaultConfig, ...configParam};

    const hasLinks = Reflect.has(obj, 'links');
    const optionalProps = hasLinks ? {links: []} : {};
    const checkResult = checkObject(obj, {...requiredProps.Kata, ...optionalProps});
    if (checkResult.isValid === false) {
      if (checkResult.missing.length > 0) {
        throw new TypeError('Missing props in kata object: ' + checkResult.missing.join(', '));
      }
      if (checkResult.wrongTypes.length > 0) {
        throw new TypeError('Wrong types in kata object, should be like so: ' + checkResult.wrongTypes.join(', '));
      }
    }

    const instance = /** @type {import('./Kata').Kata} */(new Kata());
    const isPublished = Reflect.has(obj, 'publishDateRfc822');
    instance.readableCreatedAt = '';
    if (isPublished) {
      const publishDate = new Date(obj.publishDateRfc822 ?? '');
      if (publishDate.toString() === 'Invalid Date') {
        throw new Error('Invalid date for field: "publishDateRfc822".');
      }
      instance.readableCreatedAt = new Intl.DateTimeFormat('en-GB', { dateStyle: 'long'}).format(publishDate);
    }
    const wasPublishedInTheLastThirtyDays = new Date(obj.publishDateRfc822 ?? '').getTime() > new Date().getTime() - 30 * 24 * 60 * 60 * 1000;
    instance.isNew = isPublished && wasPublishedInTheLastThirtyDays;

    instance.id = { bundle: config.bundleId, id: obj.id };
    instance.isPublished = isPublished;
    instance.name = String(obj.name);
    instance.description = String(obj.description);
    instance.path = String(obj.path);
    instance.groupName = String(obj.groupName);
    instance.level = String(obj.level);
    instance.links = extractLinks(obj?.links);
    instance.requiresKnowledgeFrom = extractRequiredKnowledge(obj.requiresKnowledgeFrom);
    instance.absolutePath = path.join('/katas', config.rootUrl, obj.path, '/');
    instance.kataId = path.join(config.rootUrl, obj.path);
    instance.url = `https://tddbin.com/#?kata=${instance.kataId}`; // should be tddbinUrl
    return instance;
  }

  /**
   * @this {import('./Kata').Kata}
   * @param kata {import('./Kata').Kata}
   * @return {boolean}
   */
  isEqual(kata) {
    return kata.name === this.name && kata.description === this.description; // is this good enough? we should compare the `id` I think ...
  }
}

/** @type {{Kata:RawKataFromMetadata, KataLink: RawKataLink, KataId: RawKataId}} */
const requiredProps = {
  KataId: {
    bundle: '',
    id: 42,
  },
  Kata: {
    id: 42,
    name: '',
    description: '',
    path: '',
    level: '',
    requiresKnowledgeFrom: [],
    groupName: '',
    groupNameSlug: '',
  },
  KataLink: {
    url: '',
    comment: '',
  },
};
/**
 * @param partial {Partial<RawKataFromMetadata>}
 * @return {RawKataFromMetadata}
 */
const oneRawKataObject = (partial = {}) => {
  return {
    ...requiredProps.Kata,
    publishDateRfc822: 'Sun, 28 Jul 2019 22:55:00 GMT',
    links: [],
    ...partial,
  };
};
/**
 * @param partial {Partial<RawKataLink>}
 * @return {RawKataLink}
 */
const oneRawKataLink = (partial = {}) => {
  return {
    ...requiredProps.KataLink,
    tags: [],
    ...partial,
  };
};
/**
 * @type {import('./Kata').ForTestingOnly}
 */
export const forTestingOnly = {
  fixtures: { oneRawKataObject, oneRawKataLink },
};
