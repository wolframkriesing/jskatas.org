// @ts-nocheck TODO make this type checked, but for getting 0 errors for now, some files dont get checked yet. Improve it!
import * as path from 'path';
import {slug} from "../_shared/slug.js";

/**
 * @param s {string}
 * @return {string}
 */
const trimExtension = (s) => {
  return s.replace(/.md$/, '');
}
/**
 * @param {KataMarkdownFile} markdownFile
 * @return {RelativeUrl}
 */
const urlFromMarkdownFile = (markdownFile) => {
  const filename = markdownFile.filename.replace(markdownFile.kataRoot, '');
  return '/' + path.join('katas', trimExtension(filename)) + '/';
};
/**
 * @param path {RelativeUrl}
 * @return {RelativeUrl}
 */
const removeLastPartOfPath = path => path.split('/').slice(0, -2).join('/') + '/';
/**
 * @param {KataMarkdownFile} markdownFile
 * @return {RelativeUrl}
 */
const urlForKataGroupFromMarkdownFile = (markdownFile) => {
  // if there would just be a pipe operator ;)
  return removeLastPartOfPath(removeLastPartOfPath(urlFromMarkdownFile(markdownFile)));
};

export class KataSite {
  /**
   * @param kataMarkdownFile {KataMarkdownFile}
   * @param rawKataSite {RawKataSite}
   * @return {KataSite}
   */
  static fromKataMarkdownFile(kataMarkdownFile, rawKataSite) {
    const raw = {
      ...rawKataSite,
      markdownFile: kataMarkdownFile,
    };
    return KataSite.withRawData(raw);
  }
  /**
   * @param raw {RawKataSite}
   * @return {KataSite}
   */
  static withRawData(raw) {
    const post = new KataSite();
    post._rawTags = raw.tags;
    post.abstract = raw.abstract;
    post.abstractAsHtml = raw.abstractAsHtml;
    post.bodyAsHtml = raw.bodyAsHtml;
    post.canonicalHint = raw.canonicalHint;
    post.canonicalUrl = raw.canonicalUrl;
    post.dateCreated = raw.dateCreated;
    post.headline = raw.headline;
    post.headlineAsHtml = raw.headlineAsHtml;
    post.markdownFile = raw.markdownFile;
    post.isDraft = raw.isDraft;
    post.oldUrls = raw.oldUrls;
    post.previewImage = raw.previewImage;
    post.vimeoId = raw.vimeoId;
    post.videoStartTime = raw.videoStartTime;
    post.youtubeId = raw.youtubeId;
    post.tests = raw.tests;
    return post;
  }
  /**
   * @return {boolean}
   */
  get hasVideo() {
    return Boolean(this.youtubeId || this.vimeoId);
  }
  /**
   * @return {RelativeUrl}
   */
  get url() {
    return urlFromMarkdownFile(this.markdownFile);
  }
  get previewImageUrl() {
    return this.url + this.previewImage;
  }
  get slug() {
    const filename = this.markdownFile.filename.split('/').slice(-1)[0];
    return trimExtension(filename);
  }
  get urlForKataGroup() {
    return urlForKataGroupFromMarkdownFile(this.markdownFile);
  }
  get dateCreated() {
    return this._dateCreated;
  }
  set dateCreated(dateCreated) {
    this._dateCreated = dateCreated;
  }
  get tags() {
    return this._rawTags.map(t => ({value: t, slug: slug(t)}));
  }
}
