import {Kata} from "./Kata.js";
import {checkObject} from "./_checkObject.js";

/**
 * @typedef {import('./KataBundle').RawKataBundleFromMetadata} RawKataBundleFromMetadata
 * @typedef {import('./KataBundle').KataBundleSiteData} KataBundleSiteData
 */

export class KataBundle {
  /**
   * @param obj {import('./KataBundle').RawKataBundleFromMetadata}
   * @param configParam {import('./KataBundle').KataBundleConfig}
   * @param siteData {KataBundleSiteData?}
   * @return {import('./KataBundle').KataBundle}
   */
  static fromRawMetadataObject(obj, configParam, siteData = {}) {
    const config = {rootUrl: '', ...configParam};

    const checkResult = checkObject(obj, oneRawKataBundleObject());
    if (checkResult.isValid === false) {
      if (checkResult.missing.length > 0) {
        throw new Error('Missing props in KataBundle object: ' + checkResult.missing.join(', '));
      }
      if (checkResult.wrongTypes.length > 0) {
        throw new Error('Wrong types in KataBundle object, should be like so: ' + checkResult.wrongTypes.join(', '));
      }
    }
    const instance = /** @type {import('./KataBundle').KataBundle} */(new KataBundle());
    instance.name = String(obj.name);
    instance.nameSlug = String(obj.nameSlug);
    instance.katas = (obj.items ?? []).map((kata) => Kata.fromRawMetadataObject(kata, { rootUrl: config.rootUrl, bundleId: config.bundleId }));
    instance.rootUrl = String(config.rootUrl);
    instance.blogPostsForKataGroups = siteData.blogPostsForKataGroups instanceof Map ? siteData.blogPostsForKataGroups : new Map();
    return instance;
  }

  /**
   * @this {import('./KataBundle').KataBundle}
   * @return {import('./KataBundle').KataGroup}
   */
  get kataGroups() {
    /** @type {PlainObject} */
    const groups = {};
    this.katas.forEach((kata) => {
      if (Array.isArray(groups?.[kata.groupName]) === false) {
        groups[kata.groupName] = [];
      }
      groups[kata.groupName].push(kata);
    });
    this.blogPostsForKataGroups.forEach((blogPosts, kataGroupName) => {
      if (Reflect.has(groups, kataGroupName)) {
        groups[kataGroupName].blogPosts = blogPosts; // use the array as an object and add `blogPosts` property
      }
    });
    return Object.values(groups).sort((a, b) => a.length < b.length ? 1 : -1);
  }
}

/**
 * @param partial {Partial<RawKataBundleFromMetadata>}
 * @return {RawKataBundleFromMetadata}
 */
const oneRawKataBundleObject = (partial = {}) => {
  return {
    name: '',
    nameSlug: '',
    items: [],
    ...partial,
  };
};

/**
 * @type {import('./KataBundle').ForTestingOnly}
 */
export const forTestingOnly = {
  fixtures: { oneRawKataBundleObject },
};
