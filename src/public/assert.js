// Goal of this file is to provide the same functionality in the browser as assert provides on nodejs.
// I tried simply `import assert from 'assert'` but this did not work.
// `npm i assert` did also not change anything.
// Using the assert module from node_modules directory directly works.
//
// "Did not work" was always indicated by assert not providing `assert.rejects` and `assert.doesNotReject`.

import assert from '../../node_modules/assert/build/assert.js';

globalThis.assert = assert;
