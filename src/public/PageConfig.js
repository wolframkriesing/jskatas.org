const STORAGE_KEY = 'pageConfig';

/**
 * @typedef {{getItem: (key: string) => string | null, setItem: (key: string, item: unknown) => void}} IStorage 
 */

export class PageConfig {
  #values = new Map();
  #storage;
  #defaultValues = new Map([
    ['autoOpenFirstKata', true],
    ['autoOpenNextKata', true],
  ]);

  /**
   * @param {{storage: IStorage}} deps
   */
  constructor({storage}) {
    this.#storage = storage;
    this.#initValuesFromStorage();
  }

  #initValuesFromStorage() {
    this.#values = this.#defaultValues;
    
    const rawStoredValues = this.#storage.getItem(STORAGE_KEY);
    if (rawStoredValues === null) {
      return;
    }
    
    const storedValues = safeJsonParse(rawStoredValues);
    if (typeof storedValues !== 'object') {
      return;
    }
    
    this.#defaultValues.forEach((_, key) => {
      if (key in storedValues) {
        this.#values.set(key, storedValues[key]);
      }
    });
  }

  #setValue(key, value) {
    if (false === this.#defaultValues.has(key)) {
      return;
    }
    this.#values.set(key, value);
    this.#storage.setItem(STORAGE_KEY, JSON.stringify(Object.fromEntries(this.#values)));
  }
  
  get isAutoOpenFirstKataOn() {
    return this.#values.get('autoOpenFirstKata');
  }
  set isAutoOpenFirstKataOn(newValue) {
    this.#setValue('autoOpenFirstKata', Boolean(newValue));
  }
  
  get isAutoOpenNextKataOn() {
    return this.#values.get('autoOpenNextKata');
  }
  set isAutoOpenNextKataOn(newValue) {
    this.#setValue('autoOpenNextKata', Boolean(newValue));
  }
}

const safeJsonParse = (maybeJson) => {
  try {
    return JSON.parse(maybeJson);
  } catch (e) {
    return {};
  }
}
