import {describe, it} from '../test.js';
import {assertThat, hasItem} from 'hamjest';
import {Tracking} from "./Tracking.js";

describe('Tracking the test progress', () => {
  it('WHEN there is no test AND tracking is called THEN just do not track', () => {
    let trackCallCount = 0;
    const track = () => trackCallCount++;
    const noTests = [];
    new Tracking({track}).trackTestProgress(noTests);
    assertThat('how often was the track-function called?', trackCallCount, 0);
  });

  describe('GIVEN the first test passes AND we have only one test', () => {
    it('WHEN it\'s the first time THEN track this as "1st Test Passed"', () => {
      let trackCalls = [];
      const track = (...args) => trackCalls.push(args);
      
      const tests = [{passed: true}];
      new Tracking({track}).trackTestProgress(tests);
      assertThat('track-function was called with ...', trackCalls[0], ['1st Test Passed']);
    });
    it('WHEN it\'s the the nth time THEN track the 1st-time event only once', () => {
      // This might happen when the user made the test fail again and then pass again.

      let trackedEventNames = [];
      const track = (eventName) => trackedEventNames.push(eventName);
      const tracking = new Tracking({track});
      tracking.trackTestProgress([{passed: true}]);
      tracking.trackTestProgress([{passed: false}]);
      tracking.trackTestProgress([{passed: true}]);
      assertThat('track-function was only called once', trackedEventNames, hasItem('1st Test Passed'));
      const numberOfTimesCalled = trackedEventNames.filter(name => name === '1st Test Passed').length;
      assertThat('track-function was only called once', numberOfTimesCalled, 1);
    });
  });

  it('WHEN there only 1 test AND it passes THEN track all the events: "1st Test Passed", "50% Tests Passed" and "100% Tests Passed"', () => {
    let trackCalls = [];
    const track = (...args) => trackCalls.push(args);
    
    const tests = [{passed: true}];
    new Tracking({track}).trackTestProgress(tests);
    assertThat(trackCalls, [
      ['1st Test Passed'],
      ['50% Tests Passed'],
      ['100% Tests Passed'],
    ]);
  });

  describe('GIVEN there are two tests', () => {
    it('WHEN the first one passes THEN track two events: "1st Test Passed" and "50% Tests Passed"', () => {
      let trackCalls = [];
      const track = (...args) => trackCalls.push(args);
      
      const tests = [{passed: true}, {passed: false}];
      new Tracking({track}).trackTestProgress(tests);
      assertThat(trackCalls, [
        ['1st Test Passed'],
        ['50% Tests Passed'],
      ]);
    });
    it('WHEN the first and second test passes THEN track event: "100% Tests Passed"', () => {
      let trackCalls = [];
      const track = (...args) => trackCalls.push(args);

      const tracking = new Tracking({track});
      tracking.trackTestProgress([{passed: true}, {passed: false}]);
      tracking.trackTestProgress([{passed: true}, {passed: true}]);
      assertThat(trackCalls, hasItem(['100% Tests Passed']));
    });
    it('WHEN only the second test passes THEN track event: "1st Test Passed" and "50% Tests Passed"', () => {
      let trackCalls = [];
      const track = (...args) => trackCalls.push(args);

      const tracking = new Tracking({track});
      tracking.trackTestProgress([{passed: false}, {passed: true}]);
      assertThat(trackCalls, [
        ['1st Test Passed'],
        ['50% Tests Passed'],
      ]);
    });

    it('WHEN all tests are passed THEN track all events always after the according track-call', () => {
      let trackCalls = [];
      const track = (...args) => trackCalls.push(args);
      
      const tracking = new Tracking({track});
      // no tracking is being done, since no test has passed yet
      tracking.trackTestProgress([{passed: false}, {passed: false}]);
      assertThat(trackCalls, []);
      
      tracking.trackTestProgress([{passed: true}, {passed: false}]);
      assertThat(trackCalls, [['1st Test Passed'], ['50% Tests Passed']]);
      
      // no changes since the last call, no new tracking call
      tracking.trackTestProgress([{passed: true}, {passed: false}]);
      assertThat(trackCalls, [['1st Test Passed'], ['50% Tests Passed']]);
      
      // make test 1 fail again, no tracking
      tracking.trackTestProgress([{passed: false}, {passed: false}]);
      assertThat(trackCalls, [['1st Test Passed'], ['50% Tests Passed']]);
      
      // pass all the tests and track the 100% additionally
      tracking.trackTestProgress([{passed: false}, {passed: true}]);
      tracking.trackTestProgress([{passed: true}, {passed: true}]);
      assertThat(trackCalls, [['1st Test Passed'], ['50% Tests Passed'], ['100% Tests Passed']]);
    });
  });

  describe('GIVEN three tests', () => {
    it('WHEN all tests pass eventually THEN track each step appropriately', () => {
      let trackCalls = [];
      const track = (...args) => trackCalls.push(args);
      
      const tracking = new Tracking({track});
      // no tracking is being done, since no test has passed yet
      tracking.trackTestProgress([{passed: false}, {passed: false}, {passed: false}]);
      assertThat(trackCalls, []);
      
      // the first test passes, track the 1st-test-passed event
      tracking.trackTestProgress([{passed: true}, {passed: false}, {passed: false}]);
      assertThat(trackCalls, [['1st Test Passed']]);
      
      // the third test passes but the first one fails again
      tracking.trackTestProgress([{passed: false}, {passed: false}, {passed: true}]);
      assertThat(trackCalls, [['1st Test Passed']]);
      
      // first+third test pass, track the 50%-tests-passed event
      tracking.trackTestProgress([{passed: true}, {passed: false}, {passed: true}]);
      assertThat(trackCalls, [['1st Test Passed'], ['50% Tests Passed']]);
      
      // all tests pass, track the 100%-tests-passed event
      tracking.trackTestProgress([{passed: true}, {passed: true}, {passed: true}]);
      assertThat(trackCalls, [['1st Test Passed'], ['50% Tests Passed'], ['100% Tests Passed']]);
    });
  });

  describe('GIVEN four tests', () => {
    it('WHEN all tests pass eventually THEN track each step appropriately', () => {
      let trackCalls = [];
      const track = (...args) => trackCalls.push(args);
      
      const tracking = new Tracking({track});
      // no tracking is being done, since no test has passed yet
      tracking.trackTestProgress([{passed: false}, {passed: false}, {passed: false}, {passed: false}]);
      assertThat(trackCalls, []);
      
      // the last test passes, track the 1st-test-passed event
      tracking.trackTestProgress([{passed: false}, {passed: false}, {passed: false}, {passed: true}]);
      assertThat(trackCalls, [['1st Test Passed']]);
      
      // the first test passes, track the 50%-tests-passed event
      tracking.trackTestProgress([{passed: true}, {passed: false}, {passed: false}, {passed: true}]);
      assertThat(trackCalls, [['1st Test Passed'], ['50% Tests Passed']]);
      
      // the second test passes, no new tracking, 50% had been tracked before
      tracking.trackTestProgress([{passed: true}, {passed: true}, {passed: false}, {passed: true}]);
      assertThat(trackCalls, [['1st Test Passed'], ['50% Tests Passed']]);
      
      // the third and fourth test pass, track the 100%-tests-passed event
      tracking.trackTestProgress([{passed: true}, {passed: true}, {passed: true}, {passed: true}]);
      assertThat(trackCalls, [['1st Test Passed'], ['50% Tests Passed'], ['100% Tests Passed']]);
    });
  });
});
