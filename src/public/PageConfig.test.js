import {assertThat} from 'hamjest';

import {describe, it} from '../test.js';
import {PageConfig} from "./PageConfig.js";

/**
 * @type {IStorage}
 */
const emptyStorage = {
  getItem: () => null,
  setItem: () => null,
};

const defaultValues = new Map([
  ['autoOpenFirstKata', true],
  ['autoOpenNextKata', true],
]);

describe('Page configuration', () => {
  describe('WHEN loading the PageConfig from a storage', () => {
    describe('AND the storage values are not "usable"', () => {
      it('AND storage has no values yet THEN set it to the default values', () => {
        const storage = {...emptyStorage, getItem: () => null};
        const pageConfig = new PageConfig({storage});
        assertThat(pageConfig.isAutoOpenFirstKataOn, true);
        assertThat(pageConfig.isAutoOpenNextKataOn, true);
      });
      it('AND storage has an empty object THEN use the defaults', () => {
        const storage = {...emptyStorage, getItem: () => '{}'};
        const pageConfig = new PageConfig({storage});
        assertThat(pageConfig.isAutoOpenFirstKataOn, true);
        assertThat(pageConfig.isAutoOpenNextKataOn, true);
      });
      it('AND storage is NOT a valid object (e.g. someone "hacked it") THEN use the defaults', () => {
        const storage = {...emptyStorage, getItem: () => '0'};
        const pageConfig = new PageConfig({storage});
        assertThat(pageConfig.isAutoOpenFirstKataOn, true);
        assertThat(pageConfig.isAutoOpenNextKataOn, true);
      });
      it('AND the storage provides other values as needed THEN ignore them', () => {
        const storage = {...emptyStorage, getItem: () => '{"randomValue": "random value"}'};
        const pageConfig = new PageConfig({storage});
        assertThat(pageConfig.isAutoOpenFirstKataOn, true);
        assertThat(pageConfig.isAutoOpenNextKataOn, true);
      });
      it('AND the storage provides invalid JSON THEN return the defaults', () => {
        const storage = {...emptyStorage, getItem: () => 'invalid JSON'};
        const pageConfig = new PageConfig({storage});
        assertThat(pageConfig.isAutoOpenFirstKataOn, true);
        assertThat(pageConfig.isAutoOpenNextKataOn, true);
      });
    });

    describe('AND the storage values are "usable"', () => {
      it('AND only one of the known values is provided THEN use it and default for the rest', () => {
        const storage = {...emptyStorage, getItem: () => JSON.stringify({autoOpenFirstKata: false})};
        const pageConfig = new PageConfig({storage});
        assertThat(pageConfig.isAutoOpenFirstKataOn, false);
        assertThat(pageConfig.isAutoOpenNextKataOn, true);
      });
      it('AND if all values are provided THEN use them', () => {
        const storage = {...emptyStorage, getItem: () => JSON.stringify({autoOpenFirstKata: false, autoOpenNextKata: false})};
        const pageConfig = new PageConfig({storage});

        assertThat(pageConfig.isAutoOpenFirstKataOn, false);
        assertThat(pageConfig.isAutoOpenNextKataOn, false);
      });
    });
  });
  
  describe('GIVEN setting a page-config value', () => {
    it('WHEN setting a valid value THEN store it AND provide it as new value', () => {
      const setItemCalls = [];
      const storage = {...emptyStorage};
      storage.setItem = (...args) => setItemCalls.push(args);
      const pageConfig = new PageConfig({storage});
      
      pageConfig.isAutoOpenFirstKataOn = false;
      assertThat(setItemCalls[0], ['pageConfig', JSON.stringify({autoOpenFirstKata: false, autoOpenNextKata: true})]);
      
      pageConfig.isAutoOpenNextKataOn = false;
      assertThat(setItemCalls[1], ['pageConfig', JSON.stringify({autoOpenFirstKata: false, autoOpenNextKata: false})]);
      
      assertThat(pageConfig.isAutoOpenFirstKataOn, false);
      assertThat(pageConfig.isAutoOpenNextKataOn, false);
    });
  });
});
