// this uses the <script type="importmap">, see _empty.html
// but since these are not yet supported in webworkers we are building this file here
// to export the assert lib as a esm module, that can be imported in the worker.
// Also the assert.js that was created by parcel is not an esm module, so we need to create this mapper.
globalThis.parcelRequire = globalThis.parcelRequire || {};
await import('https://jskatas.org/assert.js');

const props = Object.entries(Object.getOwnPropertyDescriptors(assert));
const assert_ = {};
props.forEach(([name, desc]) => {
  if (['name', 'length'].includes(name)) {
    return;
  }
  assert_[name] = desc.value;
});

export default assert_;
export const {
  equal,
  deepEqual,
  notEqual,
  notDeepEqual,
  throws,
  doesNotThrow,
  ok,
  strictEqual,
  notStrictEqual,
  deepStrictEqual,
  notDeepStrictEqual,
  fail,
  rejects,
  doesNotReject,
  ifError
} = assert_;
