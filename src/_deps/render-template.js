// @ts-nocheck TODO make this type checked, but for getting 0 errors for now, some files dont get checked yet. Improve it!
import nunjucks from "nunjucks";
import {minify} from 'html-minifier-terser';

import {TEMPLATES_DIRECTORY} from "../config.js";
import {toReadableDate, toReadableYearAndMonth, toWeekday} from "../_shared/date.js";
import {markdownToHtml} from "../_shared/markdown.js";

const nunjucksOptions = {
  autoescape: true,
  throwOnUndefined: true,
  trimBlocks: true,
  lstripBlocks: true,
};

const nunjucksEnv = new nunjucks.Environment(new nunjucks.FileSystemLoader(TEMPLATES_DIRECTORY), nunjucksOptions);
nunjucksEnv.addFilter('toReadableDate', toReadableDate);
nunjucksEnv.addFilter('toReadableYearAndMonth', toReadableYearAndMonth);
nunjucksEnv.addFilter('toWeekday', toWeekday);
nunjucksEnv.addFilter('mdToHtml', markdownToHtml);

const minifyOptions = {
  collapseWhitespace: true,
  conservativeCollapse: true,
  preserveLineBreaks: true,
};

/**
 * @param templateFilename {string}
 * @param data {PlainObject}
 * @return {Promise<string>}
 */
export const renderTemplate = async (templateFilename, data) => {
  try {
    return await minify(nunjucksEnv.render(templateFilename, data), minifyOptions);
  } catch (e) {
    const firstLinesOfStack = e.stack.split('\n').slice(0, 10).join('\n')
    console.error(`\n\nERROR rendering this page:`);
    console.error(JSON.stringify({pageUrl: data?.page?.url, templateFilename, breadCrumbs: data?.breadCrumbs}, null, 4));
    console.error(`${firstLinesOfStack}`);
    throw Error('Error rendering page');
  }
}

