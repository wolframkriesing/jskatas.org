// @ts-nocheck TODO make this type checked, but for getting 0 errors for now, some files dont get checked yet. Improve it!
import {assertThat} from 'hamjest';

import {describe, it} from '../test.js';
import {markdownToHtml} from "./markdown.js";

describe('Provide `markdownToHtml` so it can be used in a nunjucks filter', () => {
  it('WHEN passing a text without any md syntax THEN it returns it as is AND it is NOT wrapped in <p>', () => {
    assertThat(markdownToHtml('some text'), 'some text');
  });
  it('WHEN passing `source code` THEN it is enclosed in <code>', () => {
    assertThat(markdownToHtml('`source code`'), '<code>source code</code>');
  });
  it('WHEN mixing `source code` and *emphasis* THEN it is enclosed in <code> and <em>', () => {
    assertThat(markdownToHtml('`source code` and *emphasis*'), '<code>source code</code> and <em>emphasis</em>');
  });
  it('WHEN wrapping a backtick in backticks THEN wrap this one backtick in <code>', () => {
    // This is default markdown behaviour. Basically just documenting it here.
    assertThat(markdownToHtml('`` ` ``'), '<code>`</code>');
  });
});
