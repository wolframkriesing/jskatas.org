// @ts-nocheck TODO make this type checked, but for getting 0 errors for now, some files dont get checked yet. Improve it!
import {KataSiteMetadata} from '../generate-kata-sites/KataSite';
import {Token} from 'marked';

export type MetadataParseConfig = 
    {key: string, type: 'boolean'} | 
    {key: string, type: 'string'} | 
    {key: string, type: 'array', separator: string}

export function parseMetadata(token: Token, configs: MetadataParseConfig[]): KataSiteMetadata;
