//
// All the simple types, but more explicit than just "string" or just "number".
// and eventually they become more explicit, stricter, better ;).
//
type DateString = string; // can we say: 2000-01-01 as a type?
type DateTimeString = string; // can we say: "2000-01-01 10:00 CET" as a type? just like in https://schema.org/dateCreated it can also be both
type Filename = string;
type Path = string;
type RelativeUrl = string; // e.g. /blog/2000/01/01-post/
type Slug = string;

type PlainObject = {
  [key: string]: any;
};

type Tag = {
  value: string;
  slug: Slug;
}

interface Article {
  dateCreated: DateString | DateTimeString;
  oldUrls:  RelativeUrl[];
  tags:  Tag[];
  url: string;
}

type BlogPost = {
  title: string;
  url: string;
};

type KataGroupToBlogPostsMap = Map<string, BlogPost[]>;
