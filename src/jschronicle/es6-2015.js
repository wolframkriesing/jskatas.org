const TypedArray = {
  staticProperties: [
    'prototype',
    'BYTES_PER_ELEMENT',
  ],
  staticMethods: [
    'from',
    'of',
  ],
  instanceProperties: [
    'constructor',
    'buffer',
    'byteLength',
    'byteOffset',
    'length',
  ],
  instanceMethods: [
    'copyWithin',
    'entries',
    'every',
    'fill',
    'filter',
    'find',
    'findIndex',
    'forEach',
    'indexOf',
    'join',
    'keys',
    'lastIndexOf',
    'map',
    'reduce',
    'reduceRight',
    'reverse',
    'set',
    'slice',
    'some',
    'sort',
    'subarray',
    'toLocaleString',
    'toString',
    'values',
  ],
};

const meta = {
  name: 'es6',
  longName: 'ECMAScript 6',
  publishedAt: ['June', 2015],
  specUrl: 'https://262.ecma-international.org/6.0/',
};

const stats = {
  builtins: {
    global: {
      properties: [],
      methods: [],
      constructors: [
        'ArrayBuffer',
        'DataView',
        'Float32Array',
        'Float64Array',
        'Int8Array',
        'Int16Array',
        'Int32Array',
        'Proxy',
        'Promise',
        'Map',
        'Set',
        'Symbol',
        'Uint8Array',
        'Uint8ClampedArray',
        'Uint16Array',
        'Uint32Array',
        'WeakMap',
        'WeakSet',
        // 'Reflect' is not a constructor
      ],
      functions: []
    },

    Object: {
      staticProperties: [],
      staticMethods: [
        'assign',
        'getOwnPropertySymbols',
        'is',
        'setPrototypeOf',
      ],
      instanceProperties: [],
      instanceMethods: [],
    },
    Function: {
      staticProperties: [],
      staticMethods: [],
      instanceProperties: [
        'name',
      ],
      instanceMethods: [],
    },
    Symbol: {
      staticProperties: [
        'constructor',
        'prototype',
        'hasInstance',
        'isConcatSpreadable',
        'iterator',
        'match',
        'replace',
        'search',
        'species',
        'split',
        'toPrimitive',
        'toStringTag',
        'unscopables',
      ],
      staticMethods: [
        'for',
        'keyFor',
      ],
      instanceProperties: [],
      instanceMethods: [],
    },
    Number: {
      staticProperties: [
        'EPSILON',
        'MAX_SAFE_INTEGER',
        'MIN_SAFE_INTEGER',

      ],
      staticMethods: [
        'isFinite',
        'isInteger',
        'isNaN',
        'isSafeInteger',
        'parseFloat',
        'parseInt',
      ],
      instanceProperties: [],
      instanceMethods: [],
    },
    Math: {
      staticProperties: [],
      staticMethods: [
        'acosh',
        'asinh',
        'atanh',
        'cbrt',
        'clz32',
        'cosh',
        'expm1',
        'fround',
        'hypot',
        'imul',
        'log2',
        'log10',
        'log1p',
        'sign',
        'sinh',
        'tanh',
        'trunc',
      ],
      instanceProperties: [],
      instanceMethods: [],
    },
    String: {
      staticProperties: [],
      staticMethods: [
        'raw',
      ],
      instanceProperties: [],
      instanceMethods: [
        'codePointAt',
        'endsWith',
        'fromCodePoint',
        'includes',
        'normalize',
        'repeat',
        'startsWith',
      ],
    },
    RegExp: {
      staticProperties: [],
      staticMethods: [],
      instanceProperties: [
        'flags',
        'sticky',
        'unicode'
      ],
      instanceMethods: [],
    },
    Array: {
      staticProperties: [],
      staticMethods: [
        'from',
        'of',
      ],
      instanceProperties: [],
      instanceMethods: [
        'copyWithin',
        'entries',
        'fill',
        'find',
        'findIndex',
        'includes',
        'keys',
        'values',
      ],
    },
    Int8Array: TypedArray,
    Uint8Array: TypedArray,
    Uint8ClampedArray: TypedArray,
    Int16Array: TypedArray,
    Uint16Array: TypedArray,
    Int32Array: TypedArray,
    Uint32Array: TypedArray,
    Float32Array: TypedArray,
    Float64Array: TypedArray,
    Map: {
      staticProperties: [
        'prototype',
      ],
      staticMethods: [],
      instanceProperties: [
        'size',
      ],
      instanceMethods: [
        'constructor',
        'clear',
        'delete',
        'entries',
        'forEach',
        'get',
        'has',
        'keys',
        'set',
        'values',
      ],
    },
    Set: {
      staticProperties: [
        'prototype',
      ],
      staticMethods: [],
      instanceProperties: [
        'size',
      ],
      instanceMethods: [
        'constructor',
        'add',
        'clear',
        'delete',
        'entries',
        'forEach',
        'has',
        'keys',
        'values',
      ],
    },
    WeakMap: {
      staticProperties: [
        'prototype',
      ],
      staticMethods: [],
      instanceProperties: [],
      instanceMethods: [
        'constructor',
        'delete',
        'get',
        'has',
        'set',
      ],
    },
    WeakSet: {
      staticProperties: [
        'prototype',
      ],
      staticMethods: [],
      instanceProperties: [],
      instanceMethods: [
        'constructor',
        'add',
        'delete',
        'has',
      ],
    },
    ArrayBuffer: {
      staticProperties: [
        'prototype',
      ],
      staticMethods: [
        'isView',
      ],
      instanceProperties: [
        'constructor',
        'byteLength',
        'slice',
      ],
      instanceMethods: [],
    },
    DataView: {
      staticProperties: [
        'prototype',
      ],
      staticMethods: [],
      instanceProperties: [
        'constructor',
        'buffer',
        'byteLength',
        'byteOffset',
      ],
      instanceMethods: [
        'getFloat32',
        'getFloat64',
        'getInt8',
        'getInt16',
        'getInt32',
        'getUint8',
        'getUint16',
        'getUint32',
        'setFloat32',
        'setFloat64',
        'setInt8',
        'setInt16',
        'setInt32',
        'setUint8',
        'setUint16',
        'setUint32',
      ],
    },

    // Interfaces like `Iterator`, `Iterable`, ... are not listed here (yet)

    GeneratorFunction: {
      staticProperties: [
        'length',
        'prototype',
      ],
      staticMethods: [],
      instanceProperties: [
        'constructor',
        'prototype',
        'length',
        'name',
      ],
      instanceMethods: [],
    },
    Generator: {
      staticProperties: [
        'prototype',
      ],
      staticMethods: [],
      instanceProperties: [
        'constructor',
      ],
      instanceMethods: [
        'next',
        'return',
        'throw',
      ],
    },
    Promise: {
      staticProperties: [
        'prototype',
      ],
      staticMethods: [
        'all',
        'race',
        'reject',
        'resolve',
      ],
      instanceProperties: [
        'constructor',
      ],
      instanceMethods: [
        'catch',
        'then',
      ],
    },
    Reflect: {
      staticProperties: [],
      staticMethods: [
        'apply',
        'construct',
        'defineProperty',
        'deleteProperty',
        'enumerate',
        'get',
        'getOwnPropertyDescriptor',
        'getPrototypeOf',
        'has',
        'isExtensible',
        'ownKeys',
        'preventExtensions',
        'set',
        'setPrototypeOf',
      ],
      instanceProperties: [],
      instanceMethods: [],
    },
    Proxy: {
      staticProperties: [
        'revocable',
      ],
      staticMethods: [],
      instanceProperties: [
        'constructor',
      ],
      instanceMethods: [],
    },
  },
};

export {stats, meta};
