const meta = {
  name: 'es2016, es7',
  longName: 'ECMAScript 7',
  publishedAt: ['June', 2016],
  specUrl: 'https://262.ecma-international.org/7.0/',
};

const stats = {
  builtins: {
    global: {
      properties: [],
      methods: [],
      constructors: [],
      functions: []
    },
    Reflect: {
      staticProperties: [],
      staticMethods: [
        // 'enumerate' has been removed
      ],
      instanceProperties: [],
      instanceMethods: [],
    },
    Array: {
      staticProperties: [],
      staticMethods: [],
      instanceProperties: [],
      instanceMethods: [
        'includes',
      ],
    },
  },
};
export {stats, meta};
