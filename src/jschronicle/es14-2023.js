const meta = {
  name: 'es2023, es14',
  longName: 'ECMAScript 14',
  publishedAt: ['June', 2023],
  specUrl: 'https://262.ecma-international.org/14.0/',
};
const stats = {
  builtins: {
    global: {
      properties: [],
      methods: [],
      constructors: [],
      functions: []
    },
    Array: {
      staticMethods: [
        'fromAsync',
      ],
      instanceMethods: [
        'toReversed',
        'toSorted',
        'toSpliced',
        'with',
      ],
    },
  },
};
export {stats, meta};
