type ObjectStats = {
  staticProperties: string[],
  staticMethods: string[],
  instanceProperties: string[],
  instanceMethods: string[],
}

type Stats = {
  builtins: {
    
  }
}