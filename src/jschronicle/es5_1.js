const meta = {
  name: 'es5.1',
  longName: 'ECMAScript 5.1',
  publishedAt: ['June', 2011],
  specUrl: 'https://262.ecma-international.org/5.1/',
};

const stats = {
  builtins: {
    global: {
      properties: [],
      methods: [],
      constructors: [
        // JSON is not here since it is not usable as a constructor
      ],
      functions: []
    },
    Array: {
      staticProperties: [],
      staticMethods: [
        'isArray',
      ],
      instanceProperties: [],
      instanceMethods: [
        'indexOf', 'lastIndexOf', 'every', 'some', 'forEach', 'map', 'filter', 'reduce', 'reduceRight',
      ],
    },
    Boolean: {
      staticProperties: [],
      staticMethods: [],
      instanceProperties: [],
      instanceMethods: [],
    },
    Date: {
      staticProperties: [],
      staticMethods: [
        'now',
      ],
      instanceProperties: [],
      instanceMethods: [
        'toISOString', 'toJSON',
      ],
    },
    Error: {
      staticProperties: [],
      staticMethods: [],
      instanceProperties: [],
      instanceMethods: [],
    },
    EvalError: {
      staticProperties: [],
      staticMethods: [],
      instanceProperties: [],
      instanceMethods: [],
    },
    Function: {
      staticProperties: [],
      staticMethods: [],
      instanceProperties: [],
      instanceMethods: ['bind'],
    },
    JSON: {
      // new in ES5 
      staticProperties: [],
      staticMethods: [
        'parse', 'stringify',
      ],
      instanceProperties: [],
      instanceMethods: [],
    },
    Math: {
      staticProperties: [],
      staticMethods: [],
      instanceProperties: [],
      instanceMethods: [],
    },
    Number: {
      staticProperties: [],
      staticMethods: [],
      instanceProperties: [],
      instanceMethods: [],
    },
    Object: {
      staticProperties: [],
      staticMethods: [
        'getPrototypeOf', 'getOwnPropertyDescriptor', 'getOwnPropertyNames',
        'create', 'defineProperty', 'defineProperties', 'seal', 'freeze',
        'preventExtensions', 'isSealed', 'isFrozen', 'isExtensible',
        'keys'
      ],
      instanceProperties: [],
      instanceMethods: [],
    },
    RangeError: {
      staticProperties: [],
      staticMethods: [],
      instanceProperties: [],
      instanceMethods: [],
    },
    ReferenceError: {
      staticProperties: [],
      staticMethods: [],
      instanceProperties: [],
      instanceMethods: [],
    },
    RegExp: {
      staticProperties: [],
      staticMethods: [],
      instanceProperties: [],
      instanceMethods: [],
    },
    String: {
      staticProperties: [],
      staticMethods: [],
      instanceProperties: [],
      instanceMethods: [
        'trim',
      ],
    },
    SyntaxError: {
      staticProperties: [],
      staticMethods: [],
      instanceProperties: [],
      instanceMethods: [],
    },
    TypeError: {
      staticProperties: [],
      staticMethods: [],
      instanceProperties: [],
      instanceMethods: [],
    },
    URIError: {
      staticProperties: [],
      staticMethods: [],
      instanceProperties: [],
      instanceMethods: [],
    },
  },
};

export {stats, meta};
