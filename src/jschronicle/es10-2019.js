const meta = {
  name: 'es2019, es10',
  longName: 'ECMAScript 10',
  publishedAt: ['June', 2019],
  specUrl: 'https://262.ecma-international.org/10.0/',
};

const stats = {
  builtins: {
    global: {
      properties: [],
      methods: [],
      constructors: [],
      functions: []
    },
    Array: {
      instanceMethods: [
        'flat',
        'flatMap',
      ],
    },
    Object: {
      staticMethods: [
        'fromEntries',
      ],
    }
  },
};
export {stats, meta};
