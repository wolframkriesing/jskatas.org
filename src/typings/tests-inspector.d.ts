declare module 'tests-inspector' {

  type Test = { name: string, sourceCode: string }
  type Suite = { name: string, tests: Test[], suites: Suite[] }

  function inspectFile(filename: string): Promise<{ suites: Suite[] }>
}
