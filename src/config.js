import * as path from 'path';
import * as fs from 'fs';

const root = process.env.ROOT_DIRECTORY || '/app';
export const CONTENT_DIRECTORY = path.join(root, 'content');
export const BLOG_POSTS_DIRECTORY = path.join(CONTENT_DIRECTORY, 'blog');
export const TEST_CONTENT_DIRECTORY = path.join(root, 'test-content');
if (fs.existsSync(TEST_CONTENT_DIRECTORY) === false) {
  console.log(`\n❌  The directory "${TEST_CONTENT_DIRECTORY}" defined in "TEST_CONTENT_DIRECTORY" does not exist, please fix.\n❌  Stopping.`);
  process.exit(1);
}

export const KATAS_DIRECTORY = path.join(root, '_katas-cache');
export const TEMPLATES_DIRECTORY = path.join(root, 'templates');
export const OUTPUT_DIRECTORY = path.join(root, '_output');
