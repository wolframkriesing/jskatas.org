// @ts-nocheck TODO make this type checked, but for getting 0 errors for now, some files dont get checked yet. Improve it!
import * as path from 'path';
import * as marked from 'marked';
import * as fs from 'fs';

import {CONTENT_DIRECTORY, KATAS_DIRECTORY} from './config.js';
import {renderTemplate} from './_deps/render-template.js';
import {
  generateKataSites,
  generateHomePage,
  generateKatasByGroupOverviewPage,
  generateTryPage
} from "./generate-kata-sites/index.js";
import {generateBlogPosts} from "./generate-blog-sites/index.js";
import {writeOutputFile} from "./_deps/fs.js";
import {KataBundle} from "./generate-kata-sites/KataBundle.js";
import {generateJsChroniclePages} from "./render-jschronicle.js";

const navigationItems = [
  { 
    path: 'https://mastodontech.de/@wolframkriesing', 
    imgSrc: '/mastodon-icon.svg', 
    name: 'Mastodon', pagename: 'mastodon', title: 'Feedback welcome on Mastodon'
  },
  {path: '/katas/', name: 'Katas', pagename: 'kata'},
  {path: '/blog/', name: 'Blog', pagename: 'blog'},
  {path: '/about/', name: 'About', pagename: 'about'},
  {path: 'https://julenka.org', name: '🇺🇦', pagename: 'support ukraine'},
];
const defaultRenderParams = {
  navigationItems,
  site: {
    title: 'JavaScript Katas',
    subtitle: 'Continuously Learn JavaScript. Your Way.',
    domain: 'jskatas.org',
  },
};

const kataRoot = path.join(KATAS_DIRECTORY, 'katas');
const bundlePaths = [
  'es1/language',
  'es3/language',
  'es5/language',
  'es6/language',
  'es7/language',
  'es8/language',
  'es10/language',
  'es11/language',
  // 'es12/language',
  // 'es13/language',
  'es14/language',
  'es15/language',
];

const blogPostsForKataGroups = new Map([
  ['Unary Operators', [{title: 'What\'s an "Unary" Operator?', url: '/blog/2021/02/06-what-is-a-unary-operator/'}]],
  ['Number API', [{title: 'Building the parseInt() Katas', url: '/blog/2021/01/11-building-the-parseint-kata/'}]],
  ['Global Object API', [{title: 'Building the parseInt() Katas', url: '/blog/2021/01/11-building-the-parseint-kata/'}]],
  ['Array API', [{title: 'Learning JavaScript Arrays', url: '/blog/2023/09/29-learn-arrays/'}]],
  ['Strict mode', [{title: 'How to Control Strict Mode', url: '/blog/2023/10/16-control-strict-mode/'}]],
]);

const buildKataBundles = () =>
  Promise.all(
    bundlePaths.map(async (bundlePath) => {
        const bundleJson = (await import(path.join(kataRoot, bundlePath, '__all__.json'), {with: {type: "json"}})).default;
        return KataBundle.fromRawMetadataObject(bundleJson, {rootUrl: bundlePath, bundleId: bundlePath}, {blogPostsForKataGroups});
      }
    ));

const generateAboutPages = async () => {
  const content = marked.marked(await fs.promises.readFile(path.join(CONTENT_DIRECTORY, 'about/index.md'), 'utf8'));
  const renderedFile = await renderTemplate('about/index.html', {...defaultRenderParams, content, pageTitlePrefix: 'About'});
  await writeOutputFile('about/index.html', renderedFile);
}

const generateTestPages = async () => {
  const page = 'tests/runner.html';
  const renderedFile = await renderTemplate(page, {...defaultRenderParams, pageTitlePrefix: 'Test: Runner'});
  await writeOutputFile(page, renderedFile);
}

const generateRandomPage = async ({ bundles, kataRoot }) => {
  const katas = [];
  bundles.forEach((bundle) => {
    bundle.katas.forEach((kata) => {
      if (kata.level === 'BEGINNER' && kata.isPublished) katas.push(kata);
    });
  });

  const renderedFile = await renderTemplate('random.html', {...defaultRenderParams, pageTitlePrefix: 'Random', katas});
  await writeOutputFile('random/index.html', renderedFile);
};

const runAndTimeIt = async (label, fn) => {
  const paddedLabel = (label + new Array(20).fill(' ').join('')).substr(0, 25);
  console.time(paddedLabel);
  try {
    await fn();
  } catch(e) {
    console.error(e);
    process.exit(1); 
  }
  console.timeEnd(paddedLabel);
}

const sortByNewestAndItemsCount = (a, b) => {
  // Compare two arrays of katas: prioritize those with new katas, then by length
  const aNewest = a.some(kata => kata.isNew);
  const bNewest = b.some(kata => kata.isNew);
  if (aNewest !== bNewest) return aNewest ? -1 : 1;
  return b.length - a.length;  
}

const sortByNewestAndName = (a, b) => {
  if (a.isNew) {
    return -1;
  }
  if (b.isNew) {
    return 1;
  }
  return a.name.toLowerCase() < b.name.toLowerCase() ? -1 : 1
};

const buildKataGroups = ({bundles}) => {
  // the kataGroups across ALL katas, independent of their bundle
  const groups = {};
  bundles.forEach((bundle) => {
    const bundleGroups = bundle.kataGroups;
    bundleGroups.forEach((katas) => {
      const groupName = katas[0].groupName;
      if (Array.isArray(groups?.[groupName]) === false) {
        groups[groupName] = [];
      }
      groups[groupName] = [...groups[groupName], ...katas];
      groups[groupName].blogPosts = [...(groups[groupName].blogPosts ?? []), ...(katas.blogPosts ?? [])]; 
    });
  });
  for (const groupName in groups) {
    groups[groupName] = groups[groupName].sort(sortByNewestAndName);
  }
  return Object.values(groups).sort(sortByNewestAndItemsCount);  
}

console.time('Overall');
console.log('\nBuilding pages\n========');

const bundles = await buildKataBundles();
const kataGroups = buildKataGroups({bundles});
const numKatas = bundles.reduce((acc, bundle) => acc + bundle.katas.length, 0);

await runAndTimeIt('About pages', () => generateAboutPages());
await runAndTimeIt('Random page', () => generateRandomPage({ bundles, kataRoot }));

await runAndTimeIt(`Home page`, () => generateHomePage({ kataGroups, pageParams: defaultRenderParams, bundles, kataRoot }));
// await runAndTimeIt(`Try page`, () => generateTryPage({ kataGroups, pageParams: defaultRenderParams, bundles, kataRoot }));
await runAndTimeIt(`Kata groups`, () => generateKatasByGroupOverviewPage({ kataGroups, pageParams: defaultRenderParams, bundles, kataRoot }));
await runAndTimeIt(`Kata pages (${numKatas})`, () => generateKataSites({ pageParams: defaultRenderParams, bundles, kataRoot }));
await runAndTimeIt(`Blog pages`, () => generateBlogPosts({ pageParams: defaultRenderParams }));

await runAndTimeIt('JSChronicle pages', () => generateJsChroniclePages({ pageParams: defaultRenderParams }));
await runAndTimeIt('Test pages', () => generateTestPages());

console.log('-----');
console.timeEnd('Overall');
console.log('-----');
